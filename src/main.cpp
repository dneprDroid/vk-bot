#include <QApplication>
#include <QtNetwork/qnetworkproxy.h>


#include "views/authwebview.h"
#include "network/api.h"
#include "views/postlist/postlistview.h"
#include "mainwindow.h"


using namespace std;


vector<WallPostModel> posts;
API api;

void showPostView(MainWindow &mainWindow) {
    posts.push_back(WallPostModel());

    PostListView *v = new PostListView;
    v->resize(1024, 750);
    v->setAttribute( Qt::WA_DeleteOnClose );
    const auto datasource = [](void) mutable -> vector<WallPostModel>& {
        return posts;
    };
    const auto onItemChanged = [v](size_t index,
                                   const QString &text,
                                   const QList<QUrl> &urls) mutable  {
        bool reloadItem = false;
        auto post =  posts[index];
        post.text = text;
        if (!urls.isEmpty()) {
            reloadItem = true;
            post.addImagePathes(urls);
        }
        posts[index] = post;

        if (reloadItem)
            v->reloadItemAt(index);
    };

    v->setDataSource(datasource, onItemChanged);
    const auto onAddNewPost = [v]() {
        posts.push_back(WallPostModel());
        v->updateList();
    };
    v->setOnAddNewItemCallback(onAddNewPost);

    mainWindow.setCentralWidget(v);
}

int main(int argc, char *argv[])
{

    QApplication a(argc, argv);
    MainWindow mainWindow;
    AuthWebView *view = new AuthWebView;
    view->setTokenReceivedCallback([&view, &mainWindow] (const QString &token){
        api.setAccessToken(token);

        showPostView(mainWindow);
        view->close();
    });
    view->setAttribute( Qt::WA_DeleteOnClose );
    view->resize(1024, 750);

    api.fetchProxyServers([&view](const QString &ip, int port) {
        QNetworkProxy proxy;
        proxy.setType(QNetworkProxy::HttpProxy);
        proxy.setHostName(ip);
        proxy.setPort(port);
        QNetworkProxy::setApplicationProxy(proxy);

        view->openAuthPage();
    });

    mainWindow.setCentralWidget(view);
    mainWindow.show();
    return a.exec();
}
