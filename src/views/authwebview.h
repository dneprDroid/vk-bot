#ifndef AUTHWEBVIEW_H
#define AUTHWEBVIEW_H

#include <QWebEngineView>
#include <QWebEngineUrlRequestInterceptor>
#include <QWebEngineProfile>
#include <QRegularExpression>

#include <iostream>
#include <memory>

using namespace std;

typedef std::function<void(const QString &token)> OnTokenReceived;

const QString AUTH_URL = "https://oauth.vk.com/authorize?client_id=6043035&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=friends,offline,messages,docs,photos,groups&response_type=token&v=5.62";
const QString AUTH_REDIRECT_ULR = "https://oauth.vk.com/blank.html";



class RequestInterceptor : public QWebEngineUrlRequestInterceptor
{

public:
    OnTokenReceived onTokenReceived;

    explicit RequestInterceptor(QObject * parent = Q_NULLPTR) : QWebEngineUrlRequestInterceptor(parent) {}

    virtual void interceptRequest(QWebEngineUrlRequestInfo & info) {
        QString url = info.requestUrl().toString();
        qDebug() << "Request URL: " << url;

        if (!url.startsWith(AUTH_REDIRECT_ULR))
            return;

        QRegularExpression regex("access_token=(.*?)(&|$)");
        QRegularExpressionMatch match = regex.match(url);
        QString token = match.captured(1);
        qInfo() << "URL contains token: " << token;
        if (token.isEmpty())
            return;
        onTokenReceived(token);
    }
};


class AuthWebView : public QWebEngineView
{

private:
    RequestInterceptor * interceptor;
    QWebEnginePage * page;

public:
    explicit AuthWebView() : QWebEngineView() {

        interceptor = new RequestInterceptor(this);
        QWebEngineProfile * profile = new QWebEngineProfile(this);
        profile->setUrlRequestInterceptor(interceptor);
        page = new QWebEnginePage(profile, this);
        setPage(page);
    }

    void openAuthPage() {
//        QUrl url(AUTH_URL);
        QUrl url("https://oauth.vk.com/blank.html#access_token=533bacf01e11f55b536a565b57531ad114461ae8736d6506a3"); // for test
        qDebug() << "Openning url: " << url;
        setUrl(url);
    }

    void setTokenReceivedCallback(const OnTokenReceived &clb) {
        interceptor->onTokenReceived = clb;
    }

    virtual ~AuthWebView() {
        delete page;
    }
};

#endif // AUTHWEBVIEW_H
