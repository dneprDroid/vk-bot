#ifndef POSTLISTVIEW_H
#define POSTLISTVIEW_H

#include <QListView>
#include <QStandardItemModel>

#include <QLabel>
#include <QListWidget>
#include <QListWidgetItem>
#include <QPushButton>
#include <QVBoxLayout>
#include <QTextEdit>
#include <QDebug>


#include "src/models/wallpostmodel.h"
#include "postlistitemview.h"

using namespace std;

typedef function<vector<WallPostModel>& (void)> ListDataSource;
typedef function<void(void)> OnAddNewItem;


class PostListView : public QWidget
{

private:
    ListDataSource dataSource;

    QListWidget* list;
    std::vector<PostListItemView*> listItems;

    OnItemChanged onItemChanged;
    OnAddNewItem onAddNewItem;

public:

    PostListView( QWidget *parent = nullptr)
        : QWidget( parent ) {

        QLabel* title = new QLabel("Post List");
        title->setAlignment(Qt::AlignHCenter);

        list = new QListWidget(this);

        QPushButton *btnAdd = new QPushButton(this);
        btnAdd->setText("+ Add Post Item");
        connect(btnAdd, &QPushButton::clicked, this, &PostListView::handleAddPostClick);

        QPushButton *btnSend = new QPushButton(this);
        btnSend->setText("Send posts");
        connect(btnSend, &QPushButton::clicked, this, &PostListView::handleSendClick);

        QVBoxLayout* layout = new QVBoxLayout(this);
        layout->addWidget(title);
        layout->addWidget(list);
        layout->addWidget(btnAdd);
        layout->addWidget(btnSend);


        setLayout(layout);
        setAcceptDrops(true);
    }

    void setDataSource(const ListDataSource &ds,
                       const OnItemChanged &itemChanged) {
        onItemChanged = itemChanged;
        dataSource = ds;

        renderAll();
    }

    void setOnAddNewItemCallback(const OnAddNewItem &callback) {
        onAddNewItem = callback;
    }

    void updateList() {
        renderAll();
    }

    PostListItemView* renderItem(const WallPostModel &model, size_t index) {
        PostListItemView *itemView = new PostListItemView(onItemChanged, list);
        itemView->configure(model, index);
        return itemView;
    }

    void reloadItemAt(size_t index) {
        if (listItems.size() <= index)
            return;
        auto itemWidget = listItems.at(index);
        if (!dataSource) {
            qDebug() << "Datasource is empty";
            return;
        }
        const auto model = dataSource().at(index);
        itemWidget->configure(model, index);
    }

private:

    void handleSendClick() {

    }

    void handleAddPostClick() {
        if (onAddNewItem)
            onAddNewItem();
    }

    void renderAll() {
        listItems.clear();
        list->clear();

        for (size_t i=0; i<dataSource().size(); i++){
            const auto model = dataSource().at(i);
            QListWidgetItem* item = new QListWidgetItem(list);
            list->addItem(item);

            auto itemView = renderItem(model, i);
            listItems.push_back(itemView);

            item->setSizeHint(itemView->minimumSizeHint());
            list->setItemWidget(item, itemView);
        }
    }
};

#endif // POSTLISTVIEW_H
