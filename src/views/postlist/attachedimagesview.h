#ifndef ATTACHEDIMAGESVIEW_H
#define ATTACHEDIMAGESVIEW_H

#include <QListView>
#include <QStandardItemModel>

#include <QLabel>
#include <QListWidget>
#include <QListWidgetItem>
#include <QPushButton>
#include <QVBoxLayout>
#include <QTextEdit>
#include <QLabel>
#include <QBoxLayout>
#include <QHBoxLayout>
#include <QGroupBox>
#include <QDebug>



#include "src/models/wallpostmodel.h"

#define AttachedImagesViewSize (88)

class AttachedImagesView : public QWidget {

private:
    QBoxLayout *listLayout;

public:

    AttachedImagesView( QWidget *parent = nullptr)
        : QWidget( parent ) {

        QVBoxLayout* layout = new QVBoxLayout(this);

        // Title
        QLabel* title = new QLabel("Attached images");
        title->setAlignment(Qt::AlignHCenter);
        layout->addWidget(title);

        // Image list
        QGroupBox *lisWidget = new QGroupBox();
        listLayout = new QHBoxLayout();
        lisWidget->setLayout(listLayout);
        layout->addWidget(lisWidget);

        setLayout(layout);

        setFixedHeight(AttachedImagesViewSize * 2);
    }

    QWidget* renderItem(const QUrl &path, int index) {

        QLabel *imageLabel = new QLabel();
        QPixmap pic(path.toLocalFile());
        imageLabel->setFixedSize({ AttachedImagesViewSize, AttachedImagesViewSize });
        pic.scaled(imageLabel->size(), Qt::KeepAspectRatio);
        imageLabel->setScaledContents(true);
        imageLabel->setPixmap(pic);
        return imageLabel;
    }

    void configure(const WallPostModel &postModel) {

        clearLayout(listLayout);

        if (postModel.imagePathes.isEmpty()) {
            return;
        }
        for (int i=0; i<postModel.imagePathes.size(); i++) {
            const auto path = postModel.imagePathes.values()[i];
            auto itemView = renderItem(path, i);

            listLayout->addWidget(itemView);
        }
    }

private:
    void clearLayout(QLayout* layout, bool deleteWidgets = true)
    {
        while (QLayoutItem* item = layout->takeAt(0))
        {
            if (deleteWidgets)
            {
                if (QWidget* widget = item->widget())
                    widget->deleteLater();
            }
            if (QLayout* childLayout = item->layout())
                clearLayout(childLayout, deleteWidgets);
            delete item;
        }
    }
};


#endif // ATTACHEDIMAGESVIEW_H
