#ifndef POSTLISTITEMVIEW_H
#define POSTLISTITEMVIEW_H

#include <QLabel>
#include <QListWidget>
#include <QListWidgetItem>
#include <QPushButton>
#include <QVBoxLayout>
#include <QTextEdit>
#include <QDragEnterEvent>
#include <QMimeData>
#include <QDebug>


#include <iostream>

#include "src/utils.h"
#include "src/models/wallpostmodel.h"
#include "attachedimagesview.h"


static const QString PostListItemView_Name = "PostListItemView";

using namespace std;

typedef std::function<void(size_t, const QString &, const QList<QUrl>&)> OnItemChanged;

class PostListItemView : public QWidget
{

private:
    QTextEdit *editText;
    AttachedImagesView *imageList;

    size_t index;
    OnItemChanged onItemChanged;

public:
    PostListItemView(const OnItemChanged &onItemChanged,
                     QWidget* parent = nullptr) : QWidget(parent) {
        this->setObjectName(PostListItemView_Name);
        this->onItemChanged = onItemChanged;

        QVBoxLayout* layout = new QVBoxLayout(this);

        // Edit Text
        editText = new QTextEdit();
        editText->setPlaceholderText("Post text");
        layout->addWidget(editText);

        // Images List
        imageList = new AttachedImagesView();
        layout->addWidget(imageList);

        setLayout(layout);
        setAcceptDrops(true);

        connect(editText, &QTextEdit::textChanged, this, &PostListItemView::onTextChanged);
    }

    void configure(const WallPostModel &model, size_t index) {
        qDebug() << "PostListItemView::configure = {" << model.text << ", " << model.imagePathes << "}";

        this->index = index;

        if (!model.text.isEmpty())
            editText->setText(model.text);
        imageList->configure(model);
    }

private:

    void setBackgroundColor(const QColor &color) {
        auto pallette = palette();
        pallette.setColor(QPalette::Background, color);
        setPalette(pallette);
    }

    void onTextChanged() {
        const auto text = editText->toPlainText();
        onItemChanged(index, text, {});
    }

    void filterUrls(QList<QUrl> &urls) {

        for (int i=0; i<urls.size(); i++) {
            auto url = urls[i].url().toLower();

            if (url.endsWith(".png") ||
                    url.endsWith(".jpeg") ||
                    url.endsWith(".jpg") ||
                    url.endsWith(".gif") ||
                    url.endsWith(".bmp"))
                continue;
            urls.removeAt(i);
        }
    }

 protected:

    void dragEnterEvent(QDragEnterEvent *event) override {
        auto urls = event->mimeData()->urls();
        filterUrls(urls);
        qDebug() << "urls: " << urls << endl;
        if (!urls.isEmpty())
            event->acceptProposedAction();
        else {
            setBackgroundColor(Qt::red);
        }
    }

    void dragLeaveEvent(QDragLeaveEvent *event) override {
        setBackgroundColor(Qt::white);
    }

    void dropEvent(QDropEvent *event) override {
        event->acceptProposedAction();
        QList<QUrl> urls = event->mimeData()->urls();
        filterUrls(urls);

        if (!urls.isEmpty()){
            const auto text = editText->toPlainText();
            onItemChanged(index, text, urls);
        }
    }
};

#endif // POSTLISTITEMVIEW_H
