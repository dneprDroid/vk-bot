#ifndef WALLPOSTMODEL_H
#define WALLPOSTMODEL_H

#include <QString>
#include <QUrl>
#include <QList>
#include <QSet>

#include <vector>


class WallPostModel
{
public:
    QString text;
    QSet<QUrl> imagePathes;

    WallPostModel(QString text="") {
        this->text = text;
    }

    void addImagePath(const QUrl &imagePath) {
        imagePathes.insert(imagePath);
    }

    void addImagePathes(const QList<QUrl> &pathes) {
        foreach (const QUrl &path, pathes) {
            imagePathes.insert(path);
        }
    }
};

#endif // WALLPOSTMODEL_H
