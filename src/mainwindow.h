#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QQueue>

#include "views/postlist/postlistitemview.h"

class MainWindow : public QMainWindow {

public:

   MainWindow(QWidget *parent = nullptr,
              Qt::WindowFlags flags = Qt::WindowFlags())
       : QMainWindow(parent, flags) {

   }
};

#endif // MAINWINDOW_H
