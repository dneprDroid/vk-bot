#ifndef UTILS_H
#define UTILS_H

#include <QString>
#include <QMainWindow>
#include <QApplication>

namespace utils {


  QString decodeBase64(const QString &encoded) {
      QByteArray ba;
      ba.append(encoded);
      return QByteArray::fromBase64(ba);
  }

  QMainWindow* getMainWindow() {
      foreach (QWidget *w, qApp->topLevelWidgets())
          if (QMainWindow* mainWin = qobject_cast<QMainWindow*>(w))
              return mainWin;
      return nullptr;
  }
}
#endif // UTILS_H
