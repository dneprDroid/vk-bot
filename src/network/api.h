#ifndef API_H
#define API_H

#include <QtNetwork>

#include "src/utils.h"

static const QString PROXY_SERVER_URLS_ENCODED = "aHR0cDovL2Fub3RhcC5jb20vc3RhdHMvc2VydmVycy5qc29u";

typedef std::function<void(const QString&, int)> FetchProxyCallback;

class API
{
private:
    QNetworkAccessManager nam;
    QString accessToken;

public:
    API(){ }

    void setAccessToken(const QString &token) {
        accessToken = token;
    }

    void fetchProxyServers(const FetchProxyCallback &callback) {

        QString decodedUrl = utils::decodeBase64(PROXY_SERVER_URLS_ENCODED);
        qDebug() << decodedUrl;
        QUrl url(decodedUrl);

        QNetworkRequest request(url);
        request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

        QNetworkReply *reply = nam.get(request);
        while (!reply->isFinished()) {
            qApp->processEvents();
        }
        QByteArray responseData = reply->readAll();
        reply->deleteLater();
        QJsonDocument json = QJsonDocument::fromJson(responseData);

        auto serverList = json.array();

        foreach (const QJsonValue & serverInfo, serverList) {

            if (serverInfo["enabled"].toString() != "true")
                continue;
            auto ip = serverInfo["ip"].toString();
            auto port = serverInfo["port"].toInt();
            if (ip.isNull() || ip.isEmpty() || port < 0)
                continue;
            qDebug() << "Found server: " << ip << ":" << port;
            callback(ip, port);
            break;
        }
    }

};

#endif // API_H
